package com.sj.test;

import com.sj.proxy.*;

public class Client {
    public static void main(String[] args) throws Exception {
        Tank t = new Tank();//被代理对象
        InvocationHandler h = new TimeHandler(t);//代理逻辑，这里是记录时间的逻辑

        //Proxy.newProxyInstance(Moveable.class, h);//返回一个代理对象，我们不知道名字
        Moveable m = (Moveable) Proxy.newProxyInstance(Moveable.class, h);
        m.move();
    }
}

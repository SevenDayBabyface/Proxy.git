package com.sj.test;

import com.sj.proxy.Moveable;
import com.sj.proxy.Proxy2;

public class ProxyTest {
    public static void main(String[] args) throws Exception {
        Moveable m = (Moveable) Proxy2.newProxyInstance(Moveable.class);
        m.move();
    }
}

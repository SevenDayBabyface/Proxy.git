package com.sj.proxy;

import java.lang.reflect.Method;

public class TimeHandler1 {
    /**
     * 对方法进行自定义处理，在调用给定方法的前后加上记录时间的逻辑
     * 方法的调用：对一个方法的调用，必须知道这个方法的当前对象是谁，也就是，this是谁
     *
     * @param object 对哪个对象调用这个方法，即对object这个对象调用method方法
     * @param method 想调用的那个方法，在调用这个方法的之前或之后做一些事情
     */
    public void invoke(Object object, Method method) {
        long start = System.currentTimeMillis();
        System.out.println("starttime: " + start);

        try {
            method.invoke(object, new Object[]{}); //假设方法中没有参数
        } catch (Exception e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

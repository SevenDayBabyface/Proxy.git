package com.sj.proxy;

import java.lang.reflect.Method;

public class TimeHandler implements InvocationHandler {
    private Object target;//被代理对象

    public TimeHandler(Object target) {
        this.target = target;
    }

    /**
     * @param object o这里没用到。
     *               Proxy中的h.invoke(this, md)，其实this没用到，this的指向是代理对象。
     */
    public void invoke(Object object, Method method) {
        long start = System.currentTimeMillis();
        System.out.println("starttime: " + start);
        System.out.println(object.getClass().getName());//com.sj.proxy.TankTimeProxy

        try {
            method.invoke(target); //对被代理对象调用method方法
        } catch (Exception e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

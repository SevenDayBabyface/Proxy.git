package com.sj.proxy;

import java.lang.reflect.Method;

public interface InvocationHandler {
    void invoke(Object o, Method m);    //假设返回值void
}

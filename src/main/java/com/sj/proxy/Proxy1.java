package com.sj.proxy;

public class Proxy1 {
    //用来产生新的代理类
    public static Object newProxyInstance() {
        String rt = "\r\n";
        String src = "package com.sj.proxy;" + rt +
                "public class TankTimeProxy implements Moveable {" + rt +
                "    private Moveable moveable;" + rt +

                "    public TankTimeProxy(Moveable moveable) {" + rt +
                "        this.moveable = moveable;" + rt +
                "    }" + rt +

                "    public void move() {" + rt +
                "        long start = System.currentTimeMillis();" + rt +
                "        System.out.println(\"starttime: \" + start);" + rt +
                "        moveable.move();" + rt +
                "        long end = System.currentTimeMillis();" + rt +
                "        System.out.println(\"time: \" + (end - start));" + rt +
                "    }" + rt +
                "}";

        return null;
    }
}

package com.sj.proxy;

public interface Moveable {
    void move();
}

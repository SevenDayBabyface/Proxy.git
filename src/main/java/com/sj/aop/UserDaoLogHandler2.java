package com.sj.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class UserDaoLogHandler2 implements InvocationHandler {

    //target是被代理对象
    private Object target;

    public UserDaoLogHandler2(Object target) {
        this.target = target;
    }

    /**
     * 调用被代理对象实现接口UserDao的每个方法，save，delete都会调用invoke方法；
     * 1.先加自己的业务逻辑
     * 2.再调用被代理对象的对应方法；
     * 3.再加自己的业务逻辑也行。
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//        System.out.println("UserDaoLogHandler.invoke.proxy:" + proxy);//proxy是UserDaoImpl的代理类
//        System.out.println("UserDaoLogHandler.invoke.proxy.ClassName:" + proxy.getClass().getName());//proxy是UserDaoImpl的代理类
//        System.out.println("UserDaoLogHandler.target:" + target);//target是UserDaoImpl
        System.out.println(method.getName() + "----log start...");

        Object obj = method.invoke(target, args);
        System.out.println(method.getName() + "----log end.");
        return obj; //执行target.save, obj是1；执行target.delete， obj是true
    }
}

package com.sj.aop;

public class UserDaoImpl implements UserDao {
    public int save(String user) {
        System.out.println("add User: " + user);
        return 1;
    }

    public boolean delete(String user) {
        System.out.println("delete User: " + user);
        return true;
    }
}

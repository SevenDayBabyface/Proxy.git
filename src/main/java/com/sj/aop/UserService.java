package com.sj.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class UserService {
    /**
     * 1.构建被代理对象 userDao，
     * 2.把被代理对象交给handler，
     * 3.使用newProxyInstance产生代理对象，其中的参数：
     * 1）calssLoader：必须和被代理对象使用同一个calssLoader，
     * 2）被代理对象使用的接口，jdk生成的代理对象会使用同样的接口，
     * 3）代理对象调用方法的时候是由handler来处理
     */
    public static void main(String[] args) throws Exception {
        UserDao userDaoImpl = new UserDaoImpl();
        InvocationHandler invocationHandler = new UserDaoLogHandler(userDaoImpl);
        UserDao proxy = (UserDao) Proxy.newProxyInstance(userDaoImpl.getClass().getClassLoader(),
                userDaoImpl.getClass().getInterfaces(), invocationHandler);

        proxy.save("小明");
        proxy.delete("小明");
    }
    /**
     * JDK中要给一个类实现动态代理：这个类必须实现一个接口，没有实现接口的类，JDK是产生不了动态代理的。
     */
}

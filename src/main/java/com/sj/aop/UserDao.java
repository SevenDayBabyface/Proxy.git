package com.sj.aop;

public interface UserDao {
    int save(String user);
    boolean delete(String user);
}

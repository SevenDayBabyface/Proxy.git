package com.sj.aop;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class UserDaoLogHandler implements InvocationHandler {

    //target是被代理对象
    private Object target;

    public UserDaoLogHandler(Object target) {
        this.target = target;
    }

    /**
     * 调用被代理对象实现接口UserDao的每个方法（ave，delete）都会调用invoke方法；
     * 1.先加自己的业务逻辑
     * 2.再调用被代理对象的对应方法；
     * 3.再加自己的业务逻辑也行。
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println(method.getName() + "----log start...");

        /**
         * obj      是target调用method的返回值。也就是public int save(String)方法的返回值int
         * method   是要调用的被代理对象的方法
         * args     是method方法的参数
         */
        Object obj = method.invoke(target, args);
        System.out.println(method.getName() + "----log end.");
        return obj;//执行target.save，obj是1；执行target.delete，obj是true。
    }
}

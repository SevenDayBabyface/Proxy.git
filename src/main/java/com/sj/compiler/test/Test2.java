package com.sj.compiler.test;

import java.lang.reflect.Method;

public class Test2 {
    public static void main(String[] args) {
        //通过反射可以获取接口中有多少个方法
        Method[] methods = com.sj.proxy.Moveable.class.getMethods();
        for (Method m : methods) {
            System.out.println(m);//public abstract void com.sj.proxy.Moveable.move()
        }
    }
}
package com.sj.invocation;

public interface Moveable {
    void move();
}

package com.sj.invocation;

import java.lang.reflect.Method;

public class TankProxy2 implements Moveable {
    InvocationHandler handler;

    public TankProxy2(InvocationHandler handler) {
        this.handler = handler;
    }

    public void move() {
        try {
            Method md = Moveable.class.getMethod("move");
            handler.invoke(this, md);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.sj.invocation;

public class Client {
    public static void main(String[] args) {
//        Moveable tank = new Tank();
//
//        InvocationHandler handler1 = new TimeHandler1();
//        Moveable tankProxy1 = new TankProxy1(tank, handler1);
//        tankProxy1.move();
//
//        InvocationHandler handler2 = new TimeHandler2(tank);
//        Moveable tankProxy2 = new TankProxy2(handler2);
//        tankProxy2.move();
        //---------------------------------
        Moveable moveable = new Tank();
        InvocationHandler handler = new TimeHandler2(moveable);
        moveable = new TankProxy2(handler);
        moveable.move();
    }
}

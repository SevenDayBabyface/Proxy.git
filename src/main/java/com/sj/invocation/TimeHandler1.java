package com.sj.invocation;

import java.lang.reflect.Method;

public class TimeHandler1 implements InvocationHandler {
    public void invoke(Object o, Method m) {
        long start = System.currentTimeMillis();
        System.out.println("InvocationHandler,starttime: " + start);

        try {
            m.invoke(o, new Object[]{});//假设方法中没有参数
        } catch (Exception e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

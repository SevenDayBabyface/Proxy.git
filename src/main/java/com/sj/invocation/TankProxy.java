package com.sj.invocation;

import java.lang.reflect.Method;

public class TankProxy implements Moveable {
    InvocationHandler h;

    public TankProxy(InvocationHandler h) {
        this.h = h;
    }

    public void move() {
        try {
            Method md = Moveable.class.getMethod("move");
            h.invoke(this, md);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

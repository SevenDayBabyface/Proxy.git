package com.sj.invocation;

import java.lang.reflect.Method;

public class TankProxy1 implements Moveable {
    InvocationHandler handler;
    Moveable moveable;

    public TankProxy1(Moveable moveable, InvocationHandler handler) {
        this.moveable = moveable;
        this.handler = handler;
    }

    public void move() {
        try {
            Method md = Moveable.class.getMethod("move");
            handler.invoke(moveable, md);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

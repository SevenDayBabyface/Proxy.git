package com.sj.invocation;

import java.lang.reflect.Method;

public class TimeHandler2 implements InvocationHandler {
    Moveable target;//被代理对象

    public TimeHandler2(Moveable target) {
        this.target = target;
    }

    /**
     * @param o Object这里没用到
     * Proxy中的h.invoke(this, md)，其实this没用到，this的指向是代理对象。
     */
    public void invoke(Object o, Method m) {
        long start = System.currentTimeMillis();
        System.out.println("TimeHandler2.starttime: " + start);

        try {
            m.invoke(target);//对被代理对象调用m方法
        } catch (Exception e) {
            e.printStackTrace();
        }

        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

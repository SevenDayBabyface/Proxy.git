package com.sj.tank;

public class Client {
    public static void main(String[] args) {
        IMoveable tank = new Tank();
        //日志代理，记录tank.move()执行时间
        IMoveable timeProxy = new TankTimeProxy(tank);
        timeProxy.move();
        System.out.println("---------------------------------");

        //在时间外面加一层日志
        IMoveable logProxyTimeProxy = new TankLogProxy(timeProxy);
        logProxyTimeProxy.move();

        System.out.println("tank:" + tank);
        System.out.println("timeProxy:" + timeProxy);
        System.out.println("logProxyTimeProxy:" + logProxyTimeProxy);
    }
}

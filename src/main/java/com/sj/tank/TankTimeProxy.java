package com.sj.tank;

/**
 * 现在考虑功能上的叠加：先记录日志，再记录时间
 * 如果用继承方式，Tank2的代码就要改动了；或者弄个Tank4 extneds Tank2；要实现的代理功能更多的话，
 * 类就会无限制的增加下去；而且各个功能顺序可能不一样，比如先记录时间，再记录日志
 * <p>
 * 聚合的方式更容易实现：
 * TankTimeProxy和TankLogProxy都是一个Moveable类型的对象，两个之间可以互相代理
 */
public class TankTimeProxy implements IMoveable {
    private IMoveable iMoveable;

    public TankTimeProxy(IMoveable iMoveable) {
        this.iMoveable = iMoveable;
    }

    public void move() {
        long start = System.currentTimeMillis();
        System.out.println("starttime: " + start);
        iMoveable.move();
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

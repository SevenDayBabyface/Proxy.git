package com.sj.tank;

/**
 * 聚合，实现接口的方式，TankImplementingInterface也实现IMoveable接口，保存Tank类，在调用Tank.move方法的前后添加逻辑，来记录时间：
 * 继承、聚合两种方式都实现了对class Tank中move()方法的代理，哪种方法好呢？——聚合的方式好些，继承的方式不灵活
 */
public class TankImplementingInterface implements IMoveable {
    private Tank tank;

    public TankImplementingInterface(Tank tank) {
        this.tank = tank;
    }

    public void move() {
        long start = System.currentTimeMillis();
        System.out.println("starttime: " + start);
        tank.move();
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

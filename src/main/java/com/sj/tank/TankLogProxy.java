package com.sj.tank;

/**
 * 记录Tank move方法的日志代理
 */
public class TankLogProxy implements IMoveable {
    private IMoveable iMoveable;

    public TankLogProxy(IMoveable iMoveable) {
        this.iMoveable = iMoveable;
    }

    public void move() {
        System.out.println("Tank Start...");
        iMoveable.move();
        System.out.println("Tank Stop.");
    }
}

package com.sj.tank;

/**
 * 不知道Tank类的源码，不能在其中的move()方法中直接添加代码的情况下，怎么知道move方法的执行时间？
 * TankInherit继承Tank，重写其中的move()方法，在调用super.move()方法的前后添加逻辑，来记录时间：
 */
public class TankInherit extends Tank {
    @Override
    public void move() {
        long start = System.currentTimeMillis();
        super.move();
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

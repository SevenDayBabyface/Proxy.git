package com.sj.multipleMethods;

public class TankTimeProxy implements Moveable {
    private Moveable moveable;

    public TankTimeProxy(Moveable moveable) {
        this.moveable = moveable;
    }

    public void move() {
        long start = System.currentTimeMillis();
        System.out.println("starttime: " + start);
        moveable.move();
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }

    public void stop() {
        long start = System.currentTimeMillis();
        System.out.println("starttime: " + start);
        moveable.stop();
        long end = System.currentTimeMillis();
        System.out.println("time: " + (end - start));
    }
}

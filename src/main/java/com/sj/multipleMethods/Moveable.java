package com.sj.multipleMethods;

public interface Moveable {
    void move();
    void stop();
}

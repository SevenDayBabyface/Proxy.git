package com.sj.multipleMethods;

import java.util.Random;

public class Tank implements Moveable {
    public void move() {
        System.out.println("Tank Moving...");
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        System.out.println("Tank Stopping...");
    }
}

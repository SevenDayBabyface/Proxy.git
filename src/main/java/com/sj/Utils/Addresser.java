package com.sj.Utils;

import com.sj.aop.UserDao;
import com.sj.aop.UserDaoImpl;
import com.sj.aop.UserDaoLogHandler;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Addresser {

    private static Unsafe unsafe;

    static {
        try {
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            unsafe = (Unsafe) field.get(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long addressOf(Object o) throws Exception {

        Object[] array = new Object[]{o};

        long baseOffset = unsafe.arrayBaseOffset(Object[].class);
        int addressSize = unsafe.addressSize();
        long objectAddress;
        switch (addressSize) {
            case 4:
                objectAddress = unsafe.getInt(array, baseOffset);
                break;
            case 8:
                objectAddress = unsafe.getLong(array, baseOffset);
                break;
            default:
                throw new Error("unsupported address size: " + addressSize);
        }
        return (objectAddress);
    }

    public static void main(String... args) throws Exception {
        UserDao userDaoImpl = new UserDaoImpl();
        InvocationHandler invocationHandler = new UserDaoLogHandler(userDaoImpl);
        UserDao proxy = (UserDao) Proxy.newProxyInstance(userDaoImpl.getClass().getClassLoader(),
                userDaoImpl.getClass().getInterfaces(), invocationHandler);
        UserDao userDaoImpl2 = userDaoImpl;

        long address = addressOf(userDaoImpl);
        System.out.println(address);
        System.out.println(addressOf(userDaoImpl2));
        long address2 = addressOf(proxy);
        Object object = proxy;
        System.out.println(address2);
        System.out.println(addressOf(object));
//        printBytes(address, 27);
    }

    public static void printBytes(long objectAddress, int num) {
        for (long i = 0; i < num; i++) {
            int cur = unsafe.getByte(objectAddress + i);
            System.out.print((char) cur);
        }
        System.out.println();
    }
}
